package com.tom.valtech;

import static org.mockito.Mockito.*;

import org.junit.Before;
import org.junit.Test;

public class CheckoutTests {
	
	private IOutput output;
	private Checkout checkout;

	@Before
	public void setup() {
		output = mock(IOutput.class);
		checkout = new Checkout(output);	
	}

	@Test
	public void outputPrice() {
		// Given
		String itemsText = "apple,orange,apple,orange";
		
		// When
		checkout.price(itemsText);
		
		// Then
		verify(output).output("110");
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void invalidItemThrowsException() {
		// Given
		String itemsText = "apple,pear,apple,orange";
		
		// When
		checkout.price(itemsText);
	}

}
