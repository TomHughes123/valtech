package com.tom.valtech;

import static org.junit.Assert.*;

import org.junit.Test;

public class ItemTests {

	@Test
	public void appleTextToItem() {
		// Given
		String text = "apple";
		
		// When
		Item item = Item.textToItem(text);
		
		// Then 
		assertEquals(Item.APPLE, item);
	}
	
	@Test
	public void orangeTextToItem() {
		// Given
		String text = "orange";
		
		// When
		Item item = Item.textToItem(text);
		
		// Then 
		assertEquals(Item.ORANGE, item);
	}
	
	@Test
	public void invalidTextReturnsNull() {
		// Given
		String text = "notAnItem";
		
		// When
		Item item = Item.textToItem(text);
		
		// Then 
		assertNull(item);
	}
	
	@Test
	public void nullTextReturnsNull() {
		// Given
		String text = null;
		
		// When
		Item item = Item.textToItem(text);
		
		// Then 
		assertNull(item);
	}
}
