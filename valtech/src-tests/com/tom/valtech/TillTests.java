package com.tom.valtech;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TillTests {
	
	private Till till;

	@Before
	public void setup() {
		till = new Till();
	}

	@Test
	public void oneAppleCosts60() {
		// When
		int price = till.price(Item.APPLE);
		
		// Then
		assertEquals(60, price);
	}
	
	@Test
	public void oneOrangeCosts25() {
		// When
		int price = till.price(Item.ORANGE);
		
		// Then
		assertEquals(25, price);
	}
	
	@Test
	public void appleAndOrangeCosts85() {
		// When
		int price = till.price(Item.APPLE, Item.ORANGE);
		
		// Then
		assertEquals(85, price);
	}
	
	@Test
	public void twoApplesCost60SpecialOffer() {
		// When
		int price = till.price(Item.APPLE, Item.APPLE);
		
		// Then
		assertEquals(60, price);
	}
	
	@Test
	public void threeApplesCost120() {
		// When
		int price = till.price(Item.APPLE, Item.APPLE, Item.APPLE);
		
		// Then
		assertEquals(120, price);
	}
	
	@Test
	public void threeOrangesCost50SpecialOffer() {
		// When
		int price = till.price(Item.ORANGE, Item.ORANGE, Item.ORANGE);
		
		// Then
		assertEquals(50, price);
	}
	
	@Test
	public void fiveOrangesCost100() {
		// When
		int price = till.price(Item.ORANGE, Item.ORANGE, Item.ORANGE, Item.ORANGE, Item.ORANGE);
		
		// Then
		assertEquals(100, price);
	}

}
