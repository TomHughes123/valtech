package com.tom.valtech;

public class ConsoleOutput implements IOutput {

	@Override
	public void output(String text) {
		System.out.println(text);
	}

}
