package com.tom.valtech;

import static com.tom.valtech.Item.*;

public class Till {
	
	private static class ItemCount {
		private int appleCount;
		private int orangeCount;
	}

	public int price(Item... items) {
		ItemCount itemCount = countItems(items);
		int appleCost = calculateApplePrice(itemCount.appleCount); 
		int orangeCost = calculateOrangeCount(itemCount.orangeCount); 
		return appleCost + orangeCost;
	}

	private int calculateOrangeCount(int orangeCount) {
		int orangeCost = (orangeCount / 3) * (ORANGE.getPrice() * 2); 
		orangeCost += orangeCount % 3 * ORANGE.getPrice();
		return orangeCost;
	}

	private int calculateApplePrice( int appleCount) {
		int appleCost = (appleCount / 2) * APPLE.getPrice(); 
		appleCost += appleCount % 2 * APPLE.getPrice();
		return appleCost;
	}

	private ItemCount countItems(Item... items) {
		int appleCount = 0;
		int orangeCount = 0;
		
		for (Item item : items){
			if (Item.APPLE.equals(item)) {
				appleCount++;
			} else if (Item.ORANGE.equals(item)) {
				orangeCount++;
			}
		}
		
		ItemCount itemCount = new ItemCount();
		itemCount.appleCount = appleCount;
		itemCount.orangeCount = orangeCount;
		return itemCount;
	}

}
