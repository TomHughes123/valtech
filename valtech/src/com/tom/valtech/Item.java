package com.tom.valtech;

public enum Item {
	
	APPLE("apple", 60), ORANGE("orange", 25);
	
	private final int cost;
	private final String text;

	private Item(String text, int cost) {
		this.text = text;
		this.cost = cost;
	}

	public int getPrice() {
		return cost;
	}

	public static Item textToItem(String text) {
		Item found = null;
		for (Item value : values()){
			if (value.text.equals(text)){
				found = value; 
				break;
			}
		}
		return found;
	}
	
}
