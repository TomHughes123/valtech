package com.tom.valtech;

import java.util.ArrayList;
import java.util.List;

public class Checkout {
	
	private final IOutput output;
	private final Till till;

	public Checkout() {
		this(new ConsoleOutput());
	}

	public Checkout(IOutput output) {
		this.output = output;
		this.till = new Till();
	}

	public void price(String itemsText) {
		List<Item> items = parseItemList(itemsText);
		int price = price(items);
		output(price);
	}

	private void output(int price) {
		output.output(String.valueOf(price));
	}

	private int price(List<Item> items) {
		return till.price(items.toArray(new Item[items.size()]));
	}

	private List<Item> parseItemList(String itemsText) {
		List<Item> items = new ArrayList<Item>();
		String[] itemTextArray = itemsText.split(",");
		for (String itemText : itemTextArray){
			Item item = Item.textToItem(itemText);
			validateItem(itemText, item);
			items.add(item);
		}
		return items;
	}

	private void validateItem(String itemText, Item item) {
		if (item == null) {
			throw new IllegalArgumentException("Invalid item <" + itemText + ">.");
		}
	}
	
	public static void main(String[] args) {
		String items = "apple,orange,orange";
		new Checkout().price(items);
	}

}
